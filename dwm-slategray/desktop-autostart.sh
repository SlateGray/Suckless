#!/bin/bash
#Place this file in ~/.local/share/dwm/

function run {
 if ! pgrep "$1" ;
  then
    $@&
  fi
}

#setxkbmap -option caps:swapescape
# run aslstatus &
run slstatus &
sxhkd -c /home/z/.local/share/dwm/sxhkdrc &
picom -b  --config ~/.config/picom.conf
run "nm-applet"
# run "xfce4-power-manager"
run "volumeicon"
run "variety"
run "/usr/lib/xfce4/notifyd/xfce4-notifyd"
run "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
run "numlockx on"
run "emote"
run "blueberry-tray"
# run "pamac-tray"
# feh --bg-fill /usr/share/backgrounds/arcolinux/arco-wallpaper.jpg &
# run "nitrogen --restore"
