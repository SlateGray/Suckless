#!/usr/bin/env bash

# mkdir -p ~/.local/share/dwm
cp -i autostart.sh ~/.local/share/dwm/autostart.sh &&
cp -i layoutmenu.sh ~/.local/share/dwm/layoutmenu.sh &&
cp -i sxhkdrc ~/.local/share/dwm/sxhkdrc
