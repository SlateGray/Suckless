#!/bin/sh
# Tabs are important to Xmenu
# :setlocal noexpandtab if using vim/neovim

xmenu <<EOF | sh &
[]= Tiled	0
><> Floating	1
[M] Monocle	2
HHH Grid	3
TTT BStack	4
||| Columns	5
[@] Fib-Dwindle	6
EOF
